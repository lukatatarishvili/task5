@extends('layouts.app')

@section('content')
        
        <a href="javascript:history.back()" class="btn btn-dark">Go Back</a>
        
        <div class="jumbotron">
            <h1>{{$post->title}}</h1>
            <hr class="my-4">
            <p>{{$post->description}}</p>
            <p class="lead">
                <small>Created at: {{$post->created_at}}</small>
            </p>
          </div>
       
        <hr>
        <form method="POST" action="{{ action("PostsController@destroy", $post->id) }}" >
            <a href="{{$post->id}}/edit" class="btn btn-primary">Edit</a>
            <input type="submit" value="Delete" class="btn btn-danger">
            @method("DELETE")
            @csrf
        </form>
        <h3 style="text-align: center">Comments</h3>
        <hr>
        <br>
        <div class="card">
            
              @foreach ($comments as $comment)
              <div class="card-header">
                    {{$comment->comment}}

                    <div class="time" style="text-align: right">
                        <small>{{$comment->created_at}}</small>
                    </div>
              </div>
              
              @endforeach
            </div>
                
              <br>
              <form method="POST" action="{{ action("PostsController@store", $post->id) }}" >
                <input type="hidden" value="{{$post->id}}" name="post_id">
                <input type="text" name="comment" class="form-control" autofocus placeholder="დატოვე კომენტარი">
                <br>
                <input type="submit" value="დაკომენტარება" class="btn btn-success">
                @method("POST")
                @csrf
            </form>
            
              
        

       
@endsection