@extends('layouts.app')

@section('content')



<h1 style="text-align: center">Add post</h1>

        <form method="POST" action="{{ action("PostsController@update", $post->id) }}" >
        
            <div class="form-group">
                <label for="title">Title</label>
            <input type="text" name="title" id="title" class="form-control" value="{{$post->title}}">
            </div>
            
            
            <div class="form-group">
                <label for="description">Description</label>
                <textarea name="description" id="description" cols="30" class="form-control" rows="10">{{$post->description}}</textarea>
            </div>
            

            <input type="submit" class="btn btn-primary" value="submit">
            @method('PUT')
            @csrf
        </form>
    
        
@endsection