@extends('layouts.app')

@section('content')
<h1 style="text-align: center">Posts</h1>
<a href="posts/create" class="btn btn-primary" style="margin-bottom: 15px;">Create post</a>

    <div class="card">
        
                
        

        <table id="posts-table" class="table table-hover posts-table">
            <thead>
              <tr>
                <th scope="col">#</th>
                
                <th scope="col">First name</th>      
                <th scope="col">Created_at</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($posts as $post)
                <tr>
                    <td> {{$post->id}}</td>
                    <td><a href="posts/{{$post->id}}">{{$post->title}}</a> </td>
                    <td> {{$post->created_at}}</td>

                    @csrf
                    
                    
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
                
    </div>
        
    
@endsection
